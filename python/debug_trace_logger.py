# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#
#
# NOTES:
# * Serial COM not tested
#

import sys
import logging
import argparse
import platform
import re
import serial
import serial.tools.list_ports
import telnetlib
import time
import threading
import subprocess
from subprocess import PIPE, STDOUT, CREATE_NEW_CONSOLE
from datetime import datetime


log = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s: %(message)s', datefmt='%H:%M:%S', level=logging.INFO)


TELNETP_BASE = 19000


osname = platform.system()
if osname == 'Windows':
    import win32com.client
    jlink_exe = r"C:\Program Files\SEGGER\JLink\JLink.exe"
elif osname == 'Linux':
    import usb.core
    import usb.util
    jlink_exe = '/opt/SEGGER/JLink/JLinkExe'
else:
    log.info('Unknown OS {}'.format(osname))
    exit(1)


class DebugLogger:

    def __init__(self,
                 use_serial=False,
                 baudrate=1000000,
                 target_dev="nrf52840_xxAA",
                 interface_id=None
                 ):
        self._threads = []
        self._subprocesses = []
        self._console_subprocesses = []
        self._stop_event = threading.Event()

        self._interface_ids = []

        self._filters = [""]

        if use_serial:
            self._launch_serial(baudrate, interface_id)
        else:
            self._launch_rttis(target_dev, interface_id)

    def _spawn_consoles(self):
        for console_id in self._interface_ids:
            proc = subprocess.Popen([sys.executable, "-c",
                                     "import sys\n"
                                     "import os\n"
                                     "os.system('TITLE {}')\n"
                                     "for line in sys.stdin:  # poor man's `cat`\n"
                                     "    sys.stdout.write(line)\n"
                                     "    sys.stdout.flush()\n".format(console_id)
                                     ],
                                    stdin=PIPE, bufsize=1, universal_newlines=True,
                                    # assume the parent script is started from a console itself e.g.,
                                    # this code is _not_ run as a *.pyw file
                                    creationflags=CREATE_NEW_CONSOLE)
            self._console_subprocesses.append(proc)

    def _enumerate_debuggers_windows(self):
        wmi = win32com.client.GetObject("winmgmts:")
        for winusb in wmi.InstancesOf("Win32_USBHub"):
            _match = re.search(r'^USB\\VID_1366&PID_[0-9]+\\([0-9]+)',
                               winusb.DeviceId)
            if _match:
                self._interface_ids.append(int(_match.group(1)))

    def _enumerate_debuggers_linux(self):
        dev = usb.core.find(idVendor=0x1366,
                            idProduct=0x1015,
                            find_all=True)
        for cfg in dev:
            self._interface_ids.append(
                int(usb.util.get_string(cfg, cfg.iSerialNumber))
            )
        dev = usb.core.find(idVendor=0x1366,
                            idProduct=0x0101,
                            find_all=True)
        for cfg in dev:
            self._interface_ids.append(
                int(usb.util.get_string(cfg, cfg.iSerialNumber))
            )
        log.info('Debugger IDs: {}'.format(self._interface_ids))

    def _enumerate_debuggers(self):
        if osname == 'Windows':
            self._enumerate_debuggers_windows()
        elif osname == 'Linux':
            self._enumerate_debuggers_linux()

    def _open_serial(self, port, baudrate):
        for _ in range(3):
            if self._stop_event.is_set():
                return None
            try:
                log.info('Opening serial port {} ({})'.format(port, baudrate))
                s = serial.Serial(port=port, baudrate=baudrate, timeout=10)
                s.open()
                return s
            except Exception as e:
                log.exception(e)
                time.sleep(1)

    def _open_telnet(self, port):
        for _ in range(3):
            if self._stop_event.is_set():
                return None
            try:
                log.info('Opening telnet port {}'.format(port))
                return telnetlib.Telnet(host='127.0.0.1', port=port, timeout=10)
            except Exception as e:
                log.exception(e)
                time.sleep(1)

    def _launch_serial(self, baudrate, com_port=None):
        if com_port:
            self._interface_ids = [com_port]
        else:
            com_ports = serial.tools.list_ports.comports()
            self._interface_ids = [info[0] for info in com_ports]

        if len(self._interface_ids) == 0:
            log.info('No COM ports found')
            exit(0)

        self._spawn_consoles()

        for port_no in self._interface_ids:
            thread = threading.Thread(target=self._logger_thread,
                                      args=(self._open_serial(port_no, baudrate), port_no),
                                      daemon=True)
            thread.start()
            self._threads.append(thread)

    def _launch_rttis(self, target_dev, debugger_id=None):
        if debugger_id:
            self._interface_ids = [debugger_id]
        else:
            self._enumerate_debuggers()

        if len(self._interface_ids) == 0:
            log.info('No RTT debuggers found')
            exit(0)

        telnet_port = TELNETP_BASE
        for debugger in self._interface_ids:
            proc = subprocess.Popen([jlink_exe,
                                     '-SelectEmuBySN',
                                     '{}'.format(debugger),
                                     '-device',
                                     target_dev,
                                     '-speed',
                                     '30000',
                                     '-if',
                                     'swd',
                                     '-AutoConnect',
                                     '1',
                                     '-RTTTelnetPort',
                                     '{}'.format(telnet_port),
                                     '-ExitOnError',
                                     '1'],
                                    stdin=PIPE, stderr=STDOUT)
            self._subprocesses.append(proc)
            telnet_port += 1

        time.sleep(1)

        self._spawn_consoles()

        telnet_port = TELNETP_BASE
        for debugger_id in self._interface_ids:
            thread = threading.Thread(target=self._logger_thread,
                                      args=(self._open_telnet(telnet_port), debugger_id),
                                      daemon=True)
            thread.start()
            self._threads.append(thread)
            telnet_port += 1

    def _logger_thread(self, source, interface_id):
        console_proc = None
        if self._console_subprocesses:
            idx = self._interface_ids.index(interface_id)
            console_proc = self._console_subprocesses[idx]

        with open('{}_output.log'.format(interface_id), 'w', buffering=1) as outfile:
            while True:
                if source is None or self._stop_event.is_set():
                    break
                try:
                    line = source.read_until(b'\n')
                    if len(line) > 0:
                        line_str = line.decode("utf-8")
                        if any(substr in line_str for substr in self._filters):
                            # outfile.write('{}: {}'.format(
                            #     datetime.now().strftime('%Y-%m-%d %H:%M:%S'), line_str))
                            outfile.write('{}: {}'.format(time.time_ns(), line_str))

                            if console_proc.poll() is None:
                                # console_proc.stdin.write('{}: {}'.format(
                                #     datetime.now().strftime('%Y-%m-%d %H:%M:%S'), line_str))
                                console_proc.stdin.write('{}: {}'.format(time.time_ns(), line_str))
                                console_proc.stdin.flush()
                except EOFError as _:
                    pass
        log.info("Exited logger thread [{}]".format(threading.get_native_id()))

    def loop(self):
        time.sleep(1)
        try:
            while True:
                self._filters = str(input("Enter trace filter string: ")).split()
                self._filters = [""] if len(self._filters) == 0 else self._filters
                log.info('Trace filter set to {}'.format(self._filters))
        except (KeyboardInterrupt, Exception) as e:
            if not isinstance(e, KeyboardInterrupt):
                log.exception(e)

            log.info("Terminating child threads and subprocesses...")

            self._stop_event.set()
            for thread in self._threads:
                thread.join()

            for proc in self._subprocesses:
                proc.communicate(input=b'exit\n')
                try:
                    proc.wait(5)
                except subprocess.TimeoutExpired:
                    proc.kill()
                log.info("Exited JLink subprocess [{}]".format(proc.pid))


def main():
    description = 'This prints RTTI or serial port debug prints to a file with a timestamp \n\n' \
                  'Requirements\n' \
                  '============\n' \
                  'Windows:\n\n' \
                  '1) Install pywin32: pip install pywin32\n' \
                  '2) Install segger software: ' \
                  'https://www.segger.com/downloads/jlink#J-LinkSoftwareAndDocumentationPack\n\n ' \
                  'Linux:\n\n' \
                  '1) Install pyusb: sudo pip install pyusb\n' \
                  '2) Install segger software: ' \
                  'https://www.segger.com/downloads/jlink#J-LinkSoftwareAndDocumentationPack\n\n'

    parser = argparse.ArgumentParser(description=description,
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--serial', '-s', help='Use serial COM', action='store_true')

    parser.add_argument('--baudrate', '-b', help='Baudrate for serial COM', default=1000000)

    parser.add_argument('--target', '-t', help='RTTIS target device', default='nrf52840_xxAA')

    parser.add_argument('--id', help='COM port OR debugger ID to be connected '
                                     '(if omitted: all found devices)', default=None)

    args = parser.parse_args()

    instance = DebugLogger(args.serial, args.baudrate, args.target, args.id)
    instance.loop()


if __name__ == "__main__":
    main()
