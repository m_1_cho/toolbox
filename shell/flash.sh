#!/bin/bash

# To be run from WSL when tools have been installed on Windows side.
# - nrfjprog.exe for Nordic
# - commander.exe for Silabs
#
# Usage: flash.sh <target (e.g. pca10056)> [<app (e.g. testnw_app)> <serial (e.g. 440109042)>]
#

set -e

# SINK_SN="683107485"  # If it's desired separate the sink from others

TARGET=$1
APP=$2
DEV_SN=$3
MCU_FAMILY=""
BASE_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )


if [ -z "$TARGET" ]; then
    echo "Usage: build.sh <target board (e.g. pca10056)> [<app (e.g. testnw_app)>]"
    exit
fi

if [[ "$TARGET" == "pca10056" ]]; then
    MCU_FAMILY="nrf52"
elif [[ "$TARGET" == "pca10090" ]]; then
    MCU_FAMILY="nrf91"
elif [[ "$TARGET" == "tbsense2" ]]; then
    MCU_FAMILY="EFR32"
else
    echo "Target board not supported: $TARGET"
    exit
fi


# Sniff serials - works also for EFR devices utilizing segger
# Other option is to use the commander.exe --version and parse (TODO)
mapfile -t SNS < <( nrfjprog.exe -i )

cd "$BASE_DIR/app-interface"

for SN in "${SNS[@]//[$'\t\r\n ']}"
do
    if [[ -z "$DEV_SN" ]] || [[ "$DEV_SN" == "$SN" ]]; then
        if [[ -z "$SINK_SN" ]] || [[ "$SN" != "$SINK_SN" ]] || [[ "$DEV_SN" == "$SINK_SN" ]]; then
            echo "Found device $SN"

            if [ ! -z "$APP" ]; then
                echo ">>> Flashing device"
                if [[ "$MCU_FAMILY" == "EFR32" ]]; then
                    bash -c "commander.exe flash build/$TARGET/$APP/final_image_$APP.hex --address 0x0 --serialno $SN"

                else  # nRF
                    bash -c "nrfjprog.exe -f $MCU_FAMILY -s $SN --program build/$TARGET/$APP/final_image_$APP.hex --sectorerase"
                    # Extra:
                    # bash -c "nrfjprog.exe -f $MCU_FAMILY -s $SN --program build/$TARGET/$APP/final_image_$APP.hex --chiperase"
                    # bash -c "nrfjprog.exe -f $MCU_FAMILY -s $SN --reset"
                    # bash -c "nrfjprog.exe -f $MCU_FAMILY -s $SN --pinreset"

                fi

                echo ">>> FLASH OK"

            else
                echo ">>> Unlocking device"

                if [[ "$MCU_FAMILY" == "EFR32" ]]; then
                    bash -c "commander.exe device unlock --device $MCU_FAMILY --serialno $SN"

                else  # nRF
                    bash -c "nrfjprog.exe -f $MCU_FAMILY -s $SN --recover"

                fi

                echo ">>> UNLOCK OK"
            fi

        fi

    fi

done
