#!/bin/bash

# To be run in Linux environment
#
# NOTE:
# * Final image creation requires python3 with
#   - pycrypto
#   - pycryptodome
#
# Usage: build.sh <profile (e.g. nrf52840)> [<app (e.g. testnw_app)>]
#

set -e

PROFILE=$1
APP=$2
APP_OPTS=$3
TARGET=""
BASE_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
DEBUG_PINS="no"


if [ -z "$PROFILE" ]; then
    echo "Usage: build.sh <profile (e.g. nrf52840)> [<app (e.g. testnw_app)> <app options>]"
    exit
fi


if [[ "$PROFILE" == "nrf52840" ]]; then
    TARGET="pca10056"
    # TARGET="mdbt50q_rx"
    # TARGET="pan1780"
    # TARGET="pca10059"
    # TARGET="pca10112"
    # TARGET="promistel_rpi_hat"
    # TARGET="wuerth_261101102"

elif [[ "$PROFILE" == "nrf9160" ]]; then
    TARGET="pca10090"
    APP_OPTS="mac_profile=dect_nr_19_ghz"

elif [[ "$PROFILE" == "efr32_24" ]]; then
    TARGET="tbsense2"
    # TARGET="silabs_brd4253a"
    # TARGET="silabs_brd4254a"

elif [[ "$PROFILE" == "efr32xg21" ]]; then
    TARGET="silabs_brd4180b"
    # TARGET="silabs_brd4181b"

elif [[ "$PROFILE" == "efr32xg22" ]]; then
    TARGET="bgm220-ek4314a"
    # TARGET="silabs_brd4184a"

elif [[ "$PROFILE" == "nrf52832" ]]; then
    TARGET="pca10040"
    # TARGET="nrf52832_mdk_v2"
    # TARGET="ruuvitag"
    # TARGET="ublox_b204"

elif [[ "$PROFILE" == "nrf52832" ]]; then
    TARGET="pca10100"

else
    echo "Target not found for profile: $PROFILE"
    exit
fi


echo "Base directory: $BASE_DIR"


# First compile stack
echo "***************************** WP STACK *****************************"
cd "$BASE_DIR/low-energy-arm"

if [ -f "makefile" ]; then
    echo "Build legacy firmware"
    # This is prior cmake
    if [ -f "source/profiles/profile_${PROFILE}.mk" ]; then
        make profile=$PROFILE clean -j4
        make profile=$PROFILE all -j4 debug_mk=yes
    else
        make profile=${PROFILE:0:5} clean -j4
        make profile=${PROFILE:0:5} all -j4 debug_mk=yes
    fi
    APP_OPTS="binaries_path=."
elif [ -f "tools/build_profile.sh" ]; then
    echo "Run build_profile.sh"
    tools/build_profile.sh $PROFILE debug_prints=yes trace_set=base debug_pins=$DEBUG_PINS
else
    echo "Run make only"
    # This is after cmake
    cd build
    rm -rf *
    export TARGET_PROFILE=$PROFILE
    cmake .. -G "Unix Makefiles" -Ddebug_prints=yes trace_set=base -Ddebug_pins=$DEBUG_PINS
    make -j4
    cd ..
fi
echo ">>> STACK BUILD OK"


# Compile bootloader
echo "***************************** BOOTLOADER *****************************"
cd "$BASE_DIR/wm-bootloader"
if [ ! -d "build" ]; then
    echo "Run build_all_profiles.sh"
    tools/build_all_profiles.sh
    echo ">>> BOOTLOADER BUILD OK"
else
    echo "Skip (already compiled)"
fi


# Compile application
echo "***************************** APPLICATION *****************************"
if [ -z "$APP" ]; then
    echo "Skip (app not defined)"
else
    cd "$BASE_DIR/app-interface"
    make app_name=$APP target_board=$TARGET -j4 clean $APP_OPTS
    make -j4 app_name=$APP target_board=$TARGET debug_prints=yes \
        debug_output_app=segger-rtt debug_output_stack=app debug_pins=$DEBUG_PINS $APP_OPTS
    echo ">>> APP BUILD OK"
fi
