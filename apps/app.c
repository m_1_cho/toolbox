/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

/*
 * \file    app.c
 * \brief   This file is a minimal app to start the stack
 */

#include <stdlib.h>

#include "settings.h"
#include "state.h"

#include "node_configuration.h"
#include "api.h"

#include "led.h"
#include "button.h"

// INTERNAL_USE_ONLY_BEGIN
#include "debug_print.h"
// INTERNAL_USE_ONLY_END


#ifndef _ms
#define _ms(s) (s * 1000000ul)
#endif

// #define UPDATE_PERIOD       10
// #define PPS_START           1
// #define MAX_PPS             51
// #define PPS_INCR            5
// #define RESTART_TIME        10 * 1000000ul

#define MAX_LED_PER_BOARD   4u


static uint32_t     m_pps;
static app_addr_t   m_own_address;
// static uint32_t     m_last_modification;
// static uint8_t      m_cb_count;

static uint8_t tx_buf[102] = {'\x1'};


static app_lib_data_to_send_t tx_def =
{
 .bytes = &tx_buf[0],
 .num_bytes = 102,
 .delay = 0,
 .tracking_id = APP_LIB_DATA_NO_TRACKING_ID,
 .qos = APP_LIB_DATA_QOS_NORMAL,
 .flags = APP_LIB_DATA_SEND_FLAG_NONE,
 .src_endpoint = 0,
 .dest_endpoint = 0,
 .dest_address = 2,
};


uint32_t data_tx_node(void)
{
    // m_cb_count++;
    // if (m_cb_count == 0)
    // {
    //     m_pps = rand() % 3;
    //     DebugPrint_printf("PPS:%u\n", m_pps);
    // }

    if (m_pps == 0)
    {
        return _ms(1);
    }

    DebugPrint_printf("tx:%u:%lu->%lu\n", tx_buf[0], m_own_address, tx_def.dest_address);

    app_lib_data_send_res_e res;
    res = lib_data->sendData(&tx_def);
    if (res != APP_LIB_DATA_SEND_RES_SUCCESS)
    {
        DebugPrint_printf("sderr:%u\n", res);
    }

    tx_buf[0] = tx_buf[0] + 1;

    return _ms(1) / m_pps;
}


// uint32_t data_tx_node(void)
// {
//     app_lib_data_send_res_e res;
//     uint32_t now = lib_time->getTimestampS();
//     if ((now - m_last_modification > UPDATE_PERIOD) || (m_pps == PPS_START))
//     {
//         // m_pps++;
//         m_pps += PPS_INCR;
//         if (m_pps > MAX_PPS)
//         {
//             DebugPrint_printf("Period=STOP\n", m_pps);
//             m_pps = PPS_START;
//             return RESTART_TIME;
//         }
//         DebugPrint_printf("Period=%uPPS\n", m_pps);
//         m_last_modification = now;
//     }

//     DebugPrint_printf("tx:%u:%lu->%lu\n", tx_buf[0], m_own_address, tx_def.dest_address);

//     res = lib_data->sendData(&tx_def);
//     if (res != APP_LIB_DATA_SEND_RES_SUCCESS)
//     {
//         DebugPrint_printf("sderr:%u\n", res);
//     }

//     tx_buf[0] = tx_buf[0] + 1;

//     return 1000000 / m_pps;
// }


app_lib_data_receive_res_e rx_ucast(const app_lib_data_received_t * data)
{
    (void) data;
    DebugPrint_printf("rx:%lu,(%u/%u),d=%u,t=%lu\n",
                      data->src_address,
                      data->src_endpoint,
                      data->dest_endpoint,
                      data->bytes[0],
                      data->delay);

    return APP_LIB_DATA_RECEIVE_RES_HANDLED;
}


static void set_led_state(uint8_t led_id, uint8_t led_state)
{
    /* Check if requested LED is available on the board. */
    uint8_t leds_num = Led_getNumber();

    if ((led_id < MAX_LED_PER_BOARD) && (led_id < leds_num) && (leds_num > 0))
    {
        /*
         * Valid LED ID requested:
         * execute command if valid LED state received.
         */
        if (led_state == 0)
        {
            Led_set(led_id, false); /* Switch off LED. */
        }
        else if (led_state == 1)
        {
            Led_set(led_id, true); /* Switch on LED. */
        }
        else
        {
            /* Invalid LED state received : do nothing. */
        }
    }
}


static void button_pressed_handler(uint8_t button_id, button_event_e event)
{
    DebugPrint_printf("button:%u\n", button_id);

    if (button_id == 0u && m_pps != 0)
    {
        m_pps = 1;
    }
    else if (button_id == 1u && m_pps != 100)
    {
        m_pps += 50;
    }
    else if (button_id == 2u && m_pps > 0)
    {
        m_pps -= 1;
    }
    else if (button_id == 3u)
    {
        m_pps += 1;
    }

    DebugPrint_printf("PPS:%u\n", m_pps);

    for (uint8_t i = 0u; i < 4u; i++)
    {
        set_led_state(i, 0);
    }
    set_led_state(button_id, 1);
}


/**
 * \brief   Initialization callback for application
 *
 * This function is called after hardware has been initialized but the
 * stack is not yet running.
 *
 */
void App_init(const app_global_functions_t * functions)
{
    // Open public API
    API_Open(functions);
    // INTERNAL_USE_ONLY_BEGIN
    DebugPrint_init();
    Wp_registerPrintf(DebugPrint_vprintf);
    // INTERNAL_USE_ONLY_END

    // Handle operations according to device
    // lib_settings->setNodeAddress(1u);
    lib_settings->getNodeAddress(&m_own_address);

    m_pps = 1;
    uint8_t ll_mode = 0;

    switch (m_own_address)
    {
        case 1:
            if (ll_mode)
            {
                lib_settings->setNodeRole(APP_LIB_SETTINGS_ROLE_SINK | 
                                        APP_LIB_SETTINGS_ROLE_FLAG_LL);
            }
            else
            {
                lib_settings->setNodeRole(APP_LIB_SETTINGS_ROLE_SINK);
            }

            lib_data->setDataReceivedCb(rx_ucast);
            lib_data->setBcastDataReceivedCb(rx_ucast);
            break;
        default:
            // tx_def.dest_address = APP_ADDR_BROADCAST;
            // tx_def.dest_address = APP_ADDR_ANYSINK;
            tx_def.dest_address = 1u;

            if (ll_mode)
            {
                lib_settings->setNodeRole(APP_LIB_SETTINGS_ROLE_HEADNODE | 
                                          APP_LIB_SETTINGS_ROLE_FLAG_LL);
            }
            else
            {
                lib_settings->setNodeRole(APP_LIB_SETTINGS_ROLE_HEADNODE);
            }

            lib_system->setPeriodicCb(data_tx_node, _ms(10), 1000);
            lib_data->setDataReceivedCb(rx_ucast);
            lib_data->setBcastDataReceivedCb(rx_ucast);
            break;
    }
    const uint8_t key[32] =  {0};
    lib_settings->setAuthenticationKey((const uint8_t *) &key[0]);
    lib_settings->setEncryptionKey((const uint8_t *) &key[0]);
    lib_settings->setNetworkAddress(0xD8D42B);
    lib_settings->setNetworkChannel(9);

    /* Set up LED. */
    Led_init();

    /* Set up buttons. */
    Button_init();
    uint8_t num_buttons = Button_get_number();

    for (uint8_t button_id = 0; button_id < num_buttons; button_id++)
    {
        /* Register button pressed event on all user available button. */
        Button_register_for_event(button_id,
                                  BUTTON_PRESSED,
                                  button_pressed_handler);
    }

    /*
     * Start the stack.
     * This is really important step, otherwise the stack will stay stopped and
     * will not be part of any network. So the device will not be reachable
     * without reflashing it
     */
    lib_state->startStack();
}
